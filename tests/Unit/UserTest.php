<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test()
    {
        $count = User::query()->count();

        self::assertEquals(0 , $count);

        //

        $user = factory(User::class)->create();

        $user->save();

        self::assertGreaterThan(0, $user->id);
    }
}
